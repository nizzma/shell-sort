#!/usr/bin/env python3

"Sort a list of songs based on their number of plays."

import sys

def order_items(songs: list, i: int, gap: int) -> list:
    """
    Reorder the 'songs' list based on the 'i' and 'gap' parameters.

    Args:
        songs (list): A list of songs where each item is a tuple (song_name, number_of_plays).
        i (int): The starting index for reordering.
        gap (int): The gap or step size for comparing and moving elements.

    This function reorders the 'songs' list based on the 'i' and 'gap' values, following the Shell Sort algorithm.
    It compares and moves elements within the list to achieve descending order based on the number of plays.

    Returns:
        list: The reordered 'songs' list.
    """
    while i < len(songs):
        j = i + gap
        while j < len(songs):
            if songs[i][1] < songs[j][1]:
                songs[i], songs[j] = songs[j], songs[i]
            j += gap
        i += 1
    return songs

def sort_music(songs: list) -> list:
    """Implement the Shell Sort algorithm to order the dictionary based on the number of plays.

    Args:
        songs (list): A list of songs where each item is a tuple (song_name, number_of_plays).

    Implement the Shell Sort algorithm to rearrange the songs in the list. Order them in descending 
    order based on the number of plays. You should consider songs with more plays as 'higher' in terms of 
    popularity and place them towards the beginning of the list.

    Your implementation should update the 'songs' list in place, resulting in a sorted list.
    """
    gap = len(songs) // 2
    while gap > 0:
        for i in range(gap):
            songs = order_items(songs, i, gap)
        gap //= 2
    return songs

def create_dictionary(arguments):
    """
    Create a dictionary from a list of arguments.

    Args:
        arguments (list): A list of elements where every pair represents a song name and its number of plays.

    This function takes a list of arguments and converts it into a dictionary where keys are song names, and values are
    the corresponding number of plays.

    Returns:
        dict: A dictionary with song names as keys and the number of plays as values.
    """
    diccionario = {}
    i = 0
    while i < len(argumentos) - 1:
        titulo = argumentos[i]
        pistas = int(argumentos[i + 1])
        diccionario[titulo] = pistas
        i += 2
    return diccionario

def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs = create_dictionary(args)

    sorted_songs = sort_music(list(songs.items()))

    sorted_dict = dict(sorted_songs)
    print(sorted_dict)

if __name__ == '__main__':
    main()
